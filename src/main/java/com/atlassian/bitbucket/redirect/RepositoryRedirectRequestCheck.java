package com.atlassian.bitbucket.redirect;

import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scm.ScmRequest;
import com.atlassian.bitbucket.scm.ScmRequestCheck;

import javax.annotation.Nonnull;
import java.io.IOException;

public class RepositoryRedirectRequestCheck implements ScmRequestCheck {

    private final RepositoryRedirectDao redirectDao;

    public RepositoryRedirectRequestCheck(RepositoryRedirectDao redirectDao) {
        this.redirectDao = redirectDao;
    }

    @Override
    public boolean check(@Nonnull ScmRequest scmRequest) throws IOException {
        Repository repository = scmRequest.getRepository();
        if (repository != null) {
            String redirectUrl = redirectDao.getRedirectUrl(repository.getId());
            if (redirectUrl != null) {
                scmRequest.sendError("THIS REPOSITORY HAS MOVED", "Please clone from " + redirectUrl);
                return false;
            }
        }
        return true;
    }

}
