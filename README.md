# Bitbucket Server Redirect Plugin

The Bitbucket Server Redirect Plugin allows administrators to block access
to moved repositories, and notify clients of their new location.
It is configured via a simple REST API.

## Example

Say you've decided to open source one of your repositories hosted in
Bitbucket Server, and you want to move it to Bitbucket Cloud instead.
It's a PITA trying to track down every user who might have a local clone
of it and telling them to update their remotes. Instead, you can you use
this plugin to set up redirects, that will notify users of the changed
location the next time they attempt to push or pull from the repository.

Here's some example output for a redirected repository:

    stash-redirect-plugin $ git pull
    fatal: remote error: THIS REPOSITORY HAS MOVED
    Please clone from https://bitbucket.org/tpettersen/stash-redirect-plugin.git

    stash-redirect-plugin $ git remote set-url origin https://bitbucket.org/tpettersen/stash-redirect-plugin.git
    stash-redirect-plugin $ git pull
    Already up-to-date.

Easy, right?

## REST API

### GET /rest/redirect/1.0/redirects

List redirects for all repositories you have administration rights for.

### GET /rest/redirect/1.0/redirects/1

Display the redirect for the repository with id = 1

### PUT /rest/redirect/1.0/redirects/1?redirect=http://foo

Add add a redirect to http://foo for the repository with id = 1

### DELETE /rest/redirect/1.0/redirects/1

Delete the redirect for the repository with id = 1

## Notes

- You must have administrative permissions for a repository in order
  to register or view configured redirects for it. This means 
  you must be a project administrator for the project containing
  the repository, a global administrator or system administrator.

- You need the repository's id to use the above REST API. To determine
  the id of a repository, navigate to the repository in Bitbucket and
  select Settings, under Repository details, the repository's location
  on disk will be listed with the id. Here's an example of repository
  with id = 1:

        /Users/atlassian/bitbucket-home/shared/data/repositories/1